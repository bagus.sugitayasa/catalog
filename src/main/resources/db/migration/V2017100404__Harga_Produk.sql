alter table product
	add column price DECIMAL (19,2);
	
update product set price=100000.00;

alter table product 
	alter column price set not null;