/**
 * 
 */
package id.training.telkomsigma.marketplace.catalog.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import id.training.telkomsigma.marketplace.catalog.entity.Product;
import id.training.telkomsigma.marketplace.catalog.entity.ProductPhotos;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version Id: 
 */
public interface ProductPhotosDao extends PagingAndSortingRepository<ProductPhotos, String> {
	public Iterable<ProductPhotos> findByProduct(Product product);
}
