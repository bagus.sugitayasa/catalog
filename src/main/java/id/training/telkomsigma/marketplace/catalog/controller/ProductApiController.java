/**
 * 
 */
package id.training.telkomsigma.marketplace.catalog.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.training.telkomsigma.marketplace.catalog.dao.ProductDao;
import id.training.telkomsigma.marketplace.catalog.dao.ProductPhotosDao;
import id.training.telkomsigma.marketplace.catalog.entity.Product;
import id.training.telkomsigma.marketplace.catalog.entity.ProductPhotos;

/**
 * @author GusdeGita
 *
 */
@RestController 
@RequestMapping("/api/product")
public class ProductApiController {
	
	@Autowired private ProductDao productDao;
	
	@Autowired private ProductPhotosDao productPhotosDao;

	@GetMapping("/")
    public Page<Product> findProducts(Pageable page){
        return productDao.findAll(page);
    }
	
	@GetMapping("/{id}")
	public Product findById(@PathVariable("id") Product p) {
		//Product p = productDao.findOne(id) tidak diperlukan lagi, karena sudah disediakan oleh Spring Data JPA
		return p;
	}
	
	@PutMapping("/{code}/{name}")
	public void putProduct(@PathVariable String code, @PathVariable String name) {
		Product p = new Product();
		p.setCode(code);
		p.setName(name);
		productDao.save(p);
	}
	
	@GetMapping("/{id}/photos")
    public Iterable<ProductPhotos> findPhotosForProduct(@PathVariable("id") Product p){
        return productPhotosDao.findByProduct(p);
    }
}
