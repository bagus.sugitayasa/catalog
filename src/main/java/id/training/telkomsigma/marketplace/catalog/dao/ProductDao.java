/**
 * 
 */
package id.training.telkomsigma.marketplace.catalog.dao;


import id.training.telkomsigma.marketplace.catalog.entity.Product;
import org.springframework.data.repository.PagingAndSortingRepository;


/**
 * @author GusdeGita
 *
 */
public interface ProductDao extends PagingAndSortingRepository<Product, String>{

}
